package com.coherentlogic.estimize.client.core.converters;

import com.coherentlogic.coherent.data.model.core.exceptions.MethodNotSupportedException;
import com.coherentlogic.estimize.client.core.domain.Companies;
import com.coherentlogic.estimize.client.core.domain.Company;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * @deprecated No longer needed.
 */
public class CompaniesConverter implements Converter {

    @Override
    public boolean canConvert(Class type) {
        boolean result = Companies.class.equals(type);

        System.out.println("rsult: " + result);

        return result;
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        throw new MethodNotSupportedException("The marshal method is not supported.");

    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        System.out.println("unmarshal: method begins.");

        Companies companies = new Companies ();

//        reader.getAttributeNames().forEachRemaining(
//            it -> {
//                System.out.println("it: " + it);
//            }
//        );

        

        reader.moveDown(); // companies
        //reader.moveDown(); // payload
        
        System.out.println("nodeName: " + reader.getNodeName());

//        Object estimize = reader.getAttribute("payload");
//
//        System.out.println("payload: " + estimize);

        while (reader.hasMoreChildren()) {

            String name = reader.getAttribute("name");
            String ticker = reader.getAttribute("ticker");

            System.out.println("name: " + name);
            System.out.println("ticker: " + ticker);

//            String name = reader.getAttribute("estimize");
//            String ticker = reader.getAttribute("ticker");
//
//            Company company = new Company ();
//
//            company.setName(name);
//            company.setTicker(ticker);
//
//            companies.getCompanies().add(company);
//
//            reader.moveDown();
        }

        return companies;
    }

}
