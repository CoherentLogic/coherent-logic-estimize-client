package com.coherentlogic.estimize.client.core.configuration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.oxm.xstream.XStreamMarshaller;
import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.model.core.xstream.CustomMarshallingStrategy;
import com.coherentlogic.estimize.client.core.builders.QueryBuilder;
import com.coherentlogic.estimize.client.core.domain.Companies;
import com.coherentlogic.estimize.client.core.domain.Consensuses;
import com.coherentlogic.estimize.client.core.domain.Estimates;
import com.coherentlogic.estimize.client.core.domain.Releases;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

/**
 * 
 * @author <a href="support@coherentlogic.com">Support</a>
 */
@Configuration
public class GlobalConfiguration {

    static final String
        ESTIMIZE_CACHE = "estimizeCache",
        X_ESTIMIZE_KEY = "X-Estimize-Key",
        ESTIMIZE_XSTREAM_MARSHALLER = "estimizeXStreamMarshaller",
        URI = "uri";

    @Autowired(required=true)
    @Qualifier(ESTIMIZE_CACHE)
    private CacheServiceProviderSpecification<String, Object> estimizeCache;

    /**
     * The X-Estimize-Key must be configured as a bean by the developer.
     */
    @Autowired
    @Qualifier(X_ESTIMIZE_KEY)
    private String xEstimizeKey;

    /**
     * System.getenv(X_ESTIMIZE_KEY)
     */
    @Bean
    public ClientHttpRequestInterceptor getDefaultClientHttpRequestInterceptor (
        @Qualifier(X_ESTIMIZE_KEY) final String xEstimizeKey
    ) {

        return new ClientHttpRequestInterceptor () {

            @Override
            public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
                throws IOException {

                HttpRequest wrapper = new HttpRequestWrapper(request);

                wrapper.getHeaders().set(X_ESTIMIZE_KEY, xEstimizeKey);

                return execution.execute(wrapper, body);
            }
        };
    }

    @Bean
    public RestTemplate getRestTemplate (
        ClientHttpRequestInterceptor clientHttpRequestInterceptor,
        @Qualifier(ESTIMIZE_XSTREAM_MARSHALLER) XStreamMarshaller xStreamMarshaller
    ) {
        MarshallingHttpMessageConverter marshallingHttpMessageConverter = new MarshallingHttpMessageConverter ();
        marshallingHttpMessageConverter.setMarshaller(xStreamMarshaller);
        marshallingHttpMessageConverter.setUnmarshaller(xStreamMarshaller);
        marshallingHttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON));

        RestTemplate restTemplate = new RestTemplate();

        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>> ();

        messageConverters.add(marshallingHttpMessageConverter);

        restTemplate.setMessageConverters(messageConverters);

        final List<ClientHttpRequestInterceptor> interceptors = Arrays.asList(
            clientHttpRequestInterceptor
        );

        restTemplate.setInterceptors(interceptors);

        return restTemplate;
    }

    /**
     * @return
     * @throws IOException
     */
    @Bean(name=ESTIMIZE_XSTREAM_MARSHALLER)
    public XStreamMarshaller getXStreamMarshaller () throws IOException {

        XStreamMarshaller result = new XStreamMarshaller ();

        result.setMarshallingStrategy(new CustomMarshallingStrategy ());

        result.setStreamDriver(new JettisonMappedXmlDriver());
        result.setAnnotatedClasses(
            Companies.class,
            Consensuses.class,
            Estimates.class,
            Releases.class
        );

        return result;
    }

    @Bean(name=URI)
    public String getUri () {
        return "http://api.estimize.com";
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public QueryBuilder getQueryBuilder (
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String, Object> estimizeCache
    ) {
        return new QueryBuilder(restTemplate, uri, estimizeCache);
    }
}
