package com.coherentlogic.estimize.client.core.domain;

import java.math.BigDecimal;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * @deprecated No longer being used.
 *
 * @author <a href="support@coherentlogic.com">Support</a>
 */
public class SampleSummaryBean extends IdentityBean {

    private static final long serialVersionUID = -3741606937306109256L;

    static final String
        MEAN = "mean",
        HIGH = "high",
        LOW = "low",
        STANDARD_DEVIATION = "standardDeviation";

    @XStreamAlias(MEAN)
    @XStreamAsAttribute
    private BigDecimal mean;

    @XStreamAlias(HIGH)
    @XStreamAsAttribute
    private BigDecimal high;

    @XStreamAlias(LOW)
    @XStreamAsAttribute
    private BigDecimal low;

    @XStreamAlias(STANDARD_DEVIATION)
    @XStreamAsAttribute
    private BigDecimal standardDeviation;

    public BigDecimal getMean() {
        return mean;
    }

    public void setMean(BigDecimal mean) {

        BigDecimal oldValue = this.mean;

        this.mean = mean;

        firePropertyChange(MEAN, oldValue, mean);
    }

    public BigDecimal getHigh() {
        return high;
    }

    public void setHigh(BigDecimal high) {

        BigDecimal oldValue = this.high;

        this.high = high;

        firePropertyChange(HIGH, oldValue, high);
    }

    public BigDecimal getLow() {
        return low;
    }

    public void setLow(BigDecimal low) {

        BigDecimal oldValue = this.low;

        this.low = low;

        firePropertyChange(LOW, oldValue, low);
    }

    public BigDecimal getStandardDeviation() {
        return standardDeviation;
    }

    public void setStandardDeviation(BigDecimal standardDeviation) {

        BigDecimal oldValue = this.standardDeviation;

        this.standardDeviation = standardDeviation;

        firePropertyChange(STANDARD_DEVIATION, oldValue, standardDeviation);
    }
}
