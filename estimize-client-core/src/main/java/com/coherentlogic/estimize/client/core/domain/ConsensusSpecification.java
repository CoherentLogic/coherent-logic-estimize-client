package com.coherentlogic.estimize.client.core.domain;

/**
 * @author <a href="support@coherentlogic.com">Support</a>
 */
public interface ConsensusSpecification {

    static final String WALLSTREET = "wallstreet", ESTIMIZE = "estimize";

    EarningsPerShare getEarningsPerShare();

    void setEarningsPerShare(EarningsPerShare earningsPerShare);

    Revenue getRevenue();

    void setRevenue(Revenue revenue);
}
