package com.coherentlogic.estimize.client.core.domain;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;

/**
 * This bean contains an estimizeId, which is used as the id in the {@link Releases} class and as the analyst_id in the
 * {@link Estimates} class.
 *
 * @deprecated This is not going to work as intended so we need to remove it.
 *
 * @author <a href="support@coherentlogic.com">Support</a>
 */
public class EstimizeBean extends IdentityBean {

    static final String ESTIMIZE_ID = "estimizeId";

    // Releases -> "id":"5173798e810f8d0bea00005f"
    // Estimates -> "analyst_id":"53e7f1956838608fed00d6b6"
    private String estimizeId;

    public String getEstimizeId() {
        return estimizeId;
    }

    public void setEstimizeId(String estimizeId) {

        String oldValue = this.estimizeId;

        this.estimizeId = estimizeId;

        firePropertyChange(ESTIMIZE_ID, oldValue, estimizeId);
    }
}
