package com.coherentlogic.estimize.client.core.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;
import com.coherentlogic.estimize.client.core.converters.CustomBigDecimalConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 * @deprecated Will be replaced by EPS and Revenue.
 * @author <a href="support@coherentlogic.com">Support</a>
 */
public class Consensus extends IdentityBean {

    private static final long serialVersionUID = -851473099258154918L;

    static final String
        MEAN = "mean",
        HIGH = "high",
        LOW = "low",
        STANDARD_DEVIATION = "standardDeviation",
        COUNT = "count",
        UPDATED_AT = "updatedAt";

    @XStreamAlias(MEAN)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal mean;

    @XStreamAlias(HIGH)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal high;

    @XStreamAlias(LOW)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal low;

    @XStreamAlias(STANDARD_DEVIATION)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal standardDeviation;

    @XStreamAlias(COUNT)
    @XStreamAsAttribute
    private Integer count;

    @XStreamAlias(UPDATED_AT)
    @XStreamAsAttribute
    private Date updatedAt;

    public BigDecimal getMean() {
        return mean;
    }

    public void setMean(BigDecimal mean) {

        BigDecimal oldValue = this.mean;

        this.mean = mean;

        firePropertyChange(MEAN, oldValue, mean);
    }

    public BigDecimal getHigh() {
        return high;
    }

    public void setHigh(BigDecimal high) {

        BigDecimal oldValue = this.high;

        this.high = high;

        firePropertyChange(HIGH, oldValue, high);
    }

    public BigDecimal getLow() {
        return low;
    }

    public void setLow(BigDecimal low) {

        BigDecimal oldValue = this.low;

        this.low = low;

        firePropertyChange(LOW, oldValue, low);
    }

    public BigDecimal getStandardDeviation() {
        return standardDeviation;
    }

    public void setStandardDeviation(BigDecimal standardDeviation) {

        BigDecimal oldValue = this.standardDeviation;

        this.standardDeviation = standardDeviation;

        firePropertyChange(STANDARD_DEVIATION, oldValue, standardDeviation);
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {

        Integer oldValue = this.count;

        this.count = count;

        firePropertyChange(COUNT, oldValue, count);
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {

        Date oldValue = this.updatedAt;

        this.updatedAt = updatedAt;

        firePropertyChange(UPDATED_AT, oldValue, updatedAt);
    }
}
