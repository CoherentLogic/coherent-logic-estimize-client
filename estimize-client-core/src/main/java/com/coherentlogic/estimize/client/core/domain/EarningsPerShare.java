package com.coherentlogic.estimize.client.core.domain;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;
import com.coherentlogic.estimize.client.core.converters.CustomBigDecimalConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@Entity
@Table(name=EarningsPerShare.EARNINGS_PER_SHARE)
public class EarningsPerShare extends IdentityBean implements RevisionSpecification {

    private static final long serialVersionUID = 8553767192625536982L;

    static final
        String
        EARNINGS_PER_SHARE = "eps",
        MEAN = "mean",
        HIGH = "high",
        LOW = "low",
        STANDARD_DEVIATION = "standard_deviation",
        COUNT = "count",
        UPDATED_AT = "updated_at",
        REVISIONS = "revisions";

    @XStreamAlias(MEAN)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal mean;

    @XStreamAlias(HIGH)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal high;

    @XStreamAlias(LOW)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal low;

    @XStreamAlias(STANDARD_DEVIATION)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal standardDeviation;

    @XStreamAlias(COUNT)
    @XStreamAsAttribute
    private Integer count;

    @XStreamAlias(REVISIONS)
    @XStreamImplicit
    private List<Revision> revisionList;

    /**
     * @deprecated Need to change this to date.
     */
    @XStreamAlias(UPDATED_AT)
    @XStreamAsAttribute
    private String updatedAt;

    public BigDecimal getMean() {
        return mean;
    }

    public void setMean(BigDecimal mean) {

        BigDecimal oldValue = this.mean;

        this.mean = mean;

        firePropertyChange(MEAN, oldValue, mean);
    }

    public BigDecimal getHigh() {
        return high;
    }

    public void setHigh(BigDecimal high) {

        BigDecimal oldValue = this.high;

        this.high = high;

        firePropertyChange(HIGH, oldValue, high);
    }

    public BigDecimal getLow() {
        return low;
    }

    public void setLow(BigDecimal low) {

        BigDecimal oldValue = this.low;

        this.low = low;

        firePropertyChange(LOW, oldValue, low);
    }

    public BigDecimal getStandardDeviation() {
        return standardDeviation;
    }

    public void setStandardDeviation(BigDecimal standardDeviation) {

        BigDecimal oldValue = this.standardDeviation;

        this.standardDeviation = standardDeviation;

        firePropertyChange(STANDARD_DEVIATION, oldValue, standardDeviation);
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {

        Integer oldValue = this.count;

        this.count = count;

        firePropertyChange(COUNT, oldValue, count);
    }

    /**
     * @deprecated Need to change this to date.
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @deprecated Need to change this to date.
     */
    public void setUpdatedAt(String updatedAt) {

        String oldValue = this.updatedAt;

        this.updatedAt = updatedAt;

        firePropertyChange(UPDATED_AT, oldValue, updatedAt);
    }

    @Override
    public List<Revision> getRevisionList() {
        return revisionList;
    }

    @Override
    public void setRevisionList(List<Revision> revisionList) {
        this.revisionList = revisionList;
    }

    @Override
    public String toString() {
        return "EarningsPerShare [mean=" + mean + ", high=" + high + ", low=" + low + ", standardDeviation="
            + standardDeviation + ", count=" + count + ", revisionList=" + revisionList + ", updatedAt=" + updatedAt
            + "]";
    }
}
