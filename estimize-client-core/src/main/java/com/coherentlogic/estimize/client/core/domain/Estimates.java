package com.coherentlogic.estimize.client.core.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;
import com.coherentlogic.estimize.client.core.converters.EstimatesConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@Entity
@Table(name=Estimates.ESTIMATES)
@XStreamAlias(Estimates.ESTIMATES)
public class Estimates extends IdentityBean {

    private static final long serialVersionUID = -6409241252611872952L;

    public static final String ESTIMATES = "estimates", ESTIMATE_LIST = "estimateList";

    @XStreamImplicit
    @XStreamAlias(Companies.PAYLOAD)
    private List<Estimate> estimateList;

    public List<Estimate> getEstimateList() {
        return estimateList;
    }

    public void setEstimateList(List<Estimate> estimateList) {

        List<Estimate> oldValue = this.estimateList;

        this.estimateList = estimateList;

        firePropertyChange(ESTIMATE_LIST, oldValue, estimateList);
    }

    @Override
    public String toString() {
        return "Estimates [estimateList=" + estimateList + "]";
    }
}
