package com.coherentlogic.estimize.client.core.domain;

import java.util.Date;
import java.util.List;

/**
 * 
 * @deprecated We don't need this anymoe so it could be deleted.
 * 
 * @todo mean -> stdDev also appears in Consensus.
 * 
 * @author <a href="support@coherentlogic.com">Support</a>
 */
public class NestedConsensus extends SampleSummaryBean {

    private Integer count;

    private Date updatedAt;

    private List<Consensus> revisionList;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {

        Integer oldValue = this.count;

        this.count = count;

        firePropertyChange("count", oldValue, count);
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {

        Date oldValue = this.updatedAt;

        this.updatedAt = updatedAt;

        firePropertyChange("updatedAt", oldValue, updatedAt);
    }

    public List<Consensus> getRevisions() {
        return revisionList;
    }

    public void setRevisions(List<Consensus> revisionList) {

        List<Consensus> oldValue = this.revisionList;

        this.revisionList = revisionList;

        firePropertyChange("revisionList", oldValue, revisionList);
    }
}
