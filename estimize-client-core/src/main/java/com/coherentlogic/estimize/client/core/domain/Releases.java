package com.coherentlogic.estimize.client.core.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * 
 * @author <a href="support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=Releases.RELEASES)
@XStreamAlias(Releases.RELEASES)
public class Releases extends IdentityBean {

    private static final long serialVersionUID = 4751358449820164541L;

    public static final String RELEASES = "releases", RELEASE_LIST = "releaseList";

    @XStreamImplicit
    @XStreamAlias(Companies.PAYLOAD)
    private List<Release> releaseList;

    public List<Release> getReleaseList() {
        return releaseList;
    }

    public void setReleaseList(List<Release> releaseList) {

        List<Release> oldValue = this.releaseList;

        this.releaseList = releaseList;

        firePropertyChange(RELEASE_LIST, oldValue, releaseList);
    }

    @Override
    public String toString() {
        return "Releases [releaseList=" + releaseList + "]";
    }
}
