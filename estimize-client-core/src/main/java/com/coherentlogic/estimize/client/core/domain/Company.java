package com.coherentlogic.estimize.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * 
 * @author <a href="support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=Company.COMPANY)
//@XStreamAlias(Company.COMPANY)
public class Company extends IdentityBean {

    private static final long serialVersionUID = -8162873204841640615L;

    static final String COMPANY = "company", NAME = "name", TICKER = "ticker";

    @XStreamAlias(NAME)
    @XStreamAsAttribute
    private String name;

    @XStreamAlias(TICKER)
    @XStreamAsAttribute
    private String ticker;

    public String getName() {
        return name;
    }

    public void setName(String name) {

        String oldValue = this.name;

        this.name = name;

        firePropertyChange(NAME, oldValue, name);
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {

        String oldValue = this.ticker;

        this.ticker = ticker;

        firePropertyChange(TICKER, oldValue, ticker);
    }

    @Override
    public String toString() {
        return "Company [name=" + name + ", ticker=" + ticker + "]";
    }
}
