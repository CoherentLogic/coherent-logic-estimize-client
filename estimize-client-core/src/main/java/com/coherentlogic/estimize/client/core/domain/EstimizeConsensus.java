package com.coherentlogic.estimize.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 
 * @author <a href="support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=ConsensusSpecification.ESTIMIZE)
public class EstimizeConsensus implements ConsensusSpecification {

    @XStreamAlias(EarningsPerShare.EARNINGS_PER_SHARE)
    private EarningsPerShare earningsPerShare;

    @XStreamAlias(Revenue.REVENUE)
    private Revenue revenue;

    @Override
    public EarningsPerShare getEarningsPerShare() {
        return earningsPerShare;
    }

    @Override
    public void setEarningsPerShare(EarningsPerShare earningsPerShare) {
        this.earningsPerShare = earningsPerShare;
    }

    @Override
    public Revenue getRevenue() {
        return revenue;
    }

    @Override
    public void setRevenue(Revenue revenue) {
        this.revenue = revenue;
    }

    @Override
    public String toString() {
        return "EstimizeConsensus [earningsPerShare=" + earningsPerShare + ", revenue=" + revenue + "]";
    }
}
