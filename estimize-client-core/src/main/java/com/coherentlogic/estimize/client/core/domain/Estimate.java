package com.coherentlogic.estimize.client.core.domain;

import java.math.BigDecimal;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * 
 * @author <a href="support@coherentlogic.com">Support</a>
 */
public class Estimate extends IdentityBean implements SharedAnalysisSpecification {

    private static final long serialVersionUID = -1099815375588448068L;

    static final String TICKER = "ticker", USER_NAME = "username", CREATED_AT = "created_at", ANALYST_ID = "analyst_id";

    @XStreamAlias(TICKER)
    @XStreamAsAttribute
    private String ticker;

    @XStreamAlias(USER_NAME)
    @XStreamAsAttribute
    private String userName;

    /**
     * For example: 2015-10-23T08:02:13-04:00
     *
     * @todo Change the type to a date.
     * @deprecated Due to the type change requirement.
     */
    @XStreamAlias(CREATED_AT)
    @XStreamAsAttribute
    private String createdAt;

    @XStreamAlias(ANALYST_ID)
    @XStreamAsAttribute
    private String analystId;

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {

        String oldValue = this.ticker;

        this.ticker = ticker;

        firePropertyChange(TICKER, oldValue, ticker);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {

        String oldValue = this.userName;

        this.userName = userName;

        firePropertyChange(USER_NAME, oldValue, userName);
    }

    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @todo Convert this string to a date, however the date converter needs to be able to work with the following
     *  example: "2015-10-23T08:02:13-04:00".
     */
    public void setCreatedAt(String createdAt) {

        String oldValue = this.createdAt;

        this.createdAt = createdAt;

        firePropertyChange(CREATED_AT, oldValue, createdAt);
    }

    public String getAnalystId() {
        return analystId;
    }

    public void setAnalystId(String analystId) {

        String oldValue = this.analystId;

        this.analystId = analystId;

        firePropertyChange(ANALYST_ID, oldValue, analystId);
    }

    @XStreamAlias(FISCAL_YEAR)
    @XStreamAsAttribute
    private String fiscalYear;

    @XStreamAlias(FISCAL_QUARTER)
    @XStreamAsAttribute
    private int fiscalQuarter;

    @XStreamAlias(EARNINGS_PER_SHARE)
    @XStreamAsAttribute
    private BigDecimal earningsPerShare;

    @XStreamAlias(REVENUE)
    @XStreamAsAttribute
    private BigDecimal revenue;

    public String getFiscalYear() {
        return fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {

        String oldValue = this.fiscalYear;

        this.fiscalYear = fiscalYear;

        firePropertyChange("fiscalYear", oldValue, fiscalYear);
        
    }

    public int getFiscalQuarter() {
        return fiscalQuarter;
    }

    public void setFiscalQuarter(int fiscalQuarter) {

        int oldValue = this.fiscalQuarter;

        this.fiscalQuarter = fiscalQuarter;

        firePropertyChange("fiscalQuarter", oldValue, fiscalQuarter);
    }

    public BigDecimal getEarningsPerShare() {
        return earningsPerShare;
    }

    public void setEarningsPerShare(BigDecimal earningsPerShare) {

        BigDecimal oldValue = this.earningsPerShare;

        this.earningsPerShare = earningsPerShare;

        firePropertyChange("earningsPerShare", oldValue, earningsPerShare);
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {

        BigDecimal oldValue = this.revenue;

        this.revenue = revenue;

        firePropertyChange("revenue", oldValue, revenue);
    }

    @Override
    public String toString() {
        return "Estimate [ticker=" + ticker + ", userName=" + userName + ", createdAt=" + createdAt + ", analystId="
            + analystId + ", fiscalYear=" + fiscalYear + ", fiscalQuarter=" + fiscalQuarter + ", earningsPerShare="
            + earningsPerShare + ", revenue=" + revenue + "]";
    }
}
