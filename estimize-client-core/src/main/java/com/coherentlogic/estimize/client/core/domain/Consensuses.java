package com.coherentlogic.estimize.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 
 * @author <a href="support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=Consensuses.CONSENSUSES)
@XStreamAlias(Consensuses.CONSENSUSES)
public class Consensuses extends IdentityBean {

    private static final long serialVersionUID = 1805621461375357208L;

    public static final String CONSENSUS = "consensus";

    public static final String CONSENSUSES = "consensuses", CONSENSUS_LIST = "consensusList";

    @XStreamAlias(ConsensusSpecification.WALLSTREET)
    private WallStreetConsensus wallStreetConsensus;

    @XStreamAlias(ConsensusSpecification.ESTIMIZE)
    private EstimizeConsensus estimizeConsensus;

    public WallStreetConsensus getWallStreetConsensus() {
        return wallStreetConsensus;
    }

    public void setWallStreetConsensus(WallStreetConsensus wallStreetConsensus) {
        this.wallStreetConsensus = wallStreetConsensus;
    }

    public EstimizeConsensus getEstimizeConsensus() {
        return estimizeConsensus;
    }

    public void setEstimizeConsensus(EstimizeConsensus estimizeConsensus) {
        this.estimizeConsensus = estimizeConsensus;
    }

    @Override
    public String toString() {
        return "Consensuses [wallStreetConsensus=" + wallStreetConsensus + ", estimizeConsensus=" + estimizeConsensus
            + "]";
    }
}
