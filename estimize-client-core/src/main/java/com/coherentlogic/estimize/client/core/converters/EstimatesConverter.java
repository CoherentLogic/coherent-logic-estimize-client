package com.coherentlogic.estimize.client.core.converters;

import com.coherentlogic.coherent.data.model.core.exceptions.MethodNotSupportedException;
import com.coherentlogic.estimize.client.core.domain.Estimates;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class EstimatesConverter implements Converter {

    @Override
    public boolean canConvert(Class type) {
        return Estimates.class.equals(type);
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        throw new MethodNotSupportedException("The marshal method is not supported.");
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

    	throw new MethodNotSupportedException("The EstimatesConverter class is deprecated.");
//        Companies companies = new Companies ();
//
//        reader.getAttributeNames().forEachRemaining(
//        	it -> {
//        		System.out.println("it: " + it);
//        	}
//        );
//        
//        System.out.println("nodeName: " + reader.getNodeName());
//        
//        reader.moveDown();
//
//        Object estimize = reader.getAttribute("payload");
//
//        System.out.println("estimize: " + estimize);
//
//        while (reader.hasMoreChildren()) {
//
//            String name = reader.getAttribute("estimize");
//            String ticker = reader.getAttribute("ticker");
//
//            Company company = new Company ();
//
//            company.setName(name);
//            company.setTicker(ticker);
//
//            companies.getCompanyList().add(company);
//
//            reader.moveDown();
//        }
//
//        return companies;
    }

}
