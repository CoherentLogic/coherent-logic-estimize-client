package com.coherentlogic.estimize.client.core.converters;

import com.coherentlogic.coherent.data.model.core.exceptions.MethodNotSupportedException;
import com.coherentlogic.estimize.client.core.domain.SharedAnalysisBean;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class SharedAnalysisBeanConverter implements Converter {

    @Override
    public boolean canConvert(Class type) {
        return SharedAnalysisBean.class.equals(type);
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        throw new MethodNotSupportedException("The marshal method is not supported.");
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        
        new Exception("feeeeee!").printStackTrace();
        
        return null;
    }

    
}
