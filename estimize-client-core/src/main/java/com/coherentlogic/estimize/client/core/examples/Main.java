package com.coherentlogic.estimize.client.core.examples;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.coherentlogic.estimize.client.core.builders.QueryBuilder;
import com.coherentlogic.estimize.client.core.domain.Companies;

/**
 * 
 * @author <a href="support@coherentlogic.com">Support</a>
 */
@SpringBootApplication
@ComponentScan(basePackages="com.coherentlogic.estimize")
public class Main implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    @Autowired
    private QueryBuilder queryBuilder;

    @Override
    public void run(String... args) { 

        //"MSFT"

        Companies companies = queryBuilder.companies("MSFT").doGet(Companies.class);

        System.out.println("companies: " + companies);

        log.info("companies: " + companies);
    }

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
