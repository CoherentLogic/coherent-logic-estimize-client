package com.coherentlogic.estimize.client.core.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource({
    "classpath*:spring/application-context.xml",
    "classpath*:spring/jpa-beans.xml",
    "classpath*:spring/null-cache-beans.xml"
})
public class XMLConfiguration {

}
