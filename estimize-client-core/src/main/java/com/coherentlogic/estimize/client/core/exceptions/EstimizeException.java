package com.coherentlogic.estimize.client.core.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * 
 * @author <a href="support@coherentlogic.com">Support</a>
 */
public class EstimizeException extends NestedRuntimeException {

    public EstimizeException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public EstimizeException(String msg) {
        super(msg);
    }
}
