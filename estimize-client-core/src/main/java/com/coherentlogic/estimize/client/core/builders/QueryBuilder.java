package com.coherentlogic.estimize.client.core.builders;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.UriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.xml.MarshallingHttpMessageConverter;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.model.core.builders.rest.AbstractRESTQueryBuilder;
import com.coherentlogic.coherent.data.model.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.estimize.client.core.domain.Companies;
import com.coherentlogic.estimize.client.core.domain.Consensuses;
import com.coherentlogic.estimize.client.core.domain.Estimates;
import com.coherentlogic.estimize.client.core.domain.Releases;
import com.coherentlogic.estimize.client.core.exceptions.UnknownTypeException;
import com.thoughtworks.xstream.converters.ConversionException;

/**
 * The QueryBuilder must always be scoped as a prototype bean as it is fire-and-forget: you use one instance of this
 * class to make exactly one call to Estimize and then the object is not reused.
 *
 * Contact <a href="mailto:sales@estimize.com">Estimize sales</a> to acquire a key which is required in order to use
 * this framework.
 *
 * @author <a href="support@coherentlogic.com">Support</a>
 */
public class QueryBuilder extends AbstractRESTQueryBuilder<String> {

    private static final Logger log = LoggerFactory.getLogger(QueryBuilder.class);

    static {
        log.warn("***********************************************************");
        log.warn("***                                                     ***");
        log.warn("***    Welcome to the Coherent Logic Estimize Client    ***");
        log.warn("***               version 0.8.5-RELEASE.                ***");
        log.warn("***                                                     ***");
        log.warn("***                Follow us on LinkedIn:               ***");
        log.warn("***                                                     ***");
        log.warn("***       https://www.linkedin.com/company/229316       ***");
        log.warn("***                                                     ***");
        log.warn("***                Follow us on Twitter:                ***");
        log.warn("***                                                     ***");
        log.warn("***         https://twitter.com/CoherentMktData         ***");
        log.warn("***                                                     ***");
        log.warn("***********************************************************");
    }

    private final Map<Class<?>, ResponseExtractor<?>> responseExtractorMap =
        new HashMap<Class<?>, ResponseExtractor<?>> ();

    public QueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String, Object> cache
    ) {
        super(restTemplate, uri, cache);
        initialize ();
    }

    public QueryBuilder(RestTemplate restTemplate, String uri) {
        super(restTemplate, uri);
        initialize ();
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<String, Object> cache
    ) {
        super(restTemplate, uriBuilder, cache);
        initialize ();
    }

    public QueryBuilder(RestTemplate restTemplate, UriBuilder uriBuilder) {
        super(restTemplate, uriBuilder);
        initialize ();
    }

    void initialize () {
        responseExtractorMap.put(Companies.class, new CompaniesResponseExtractor ());
        responseExtractorMap.put(Consensuses.class, new ConsensusesResponseExtractor ());
        responseExtractorMap.put(Estimates.class, new EstimatesResponseExtractor ());
        responseExtractorMap.put(Releases.class, new ReleasesResponseExtractor ());
    }

    /**
     * Queries Estimize for the company information that are covered.
     *
     * For example:
     * 
     * <pre>
     *     Companies companies = queryBuilder.companies().doGet(Companies.class);
     * </pre>
     */
    public QueryBuilder companies () {

        extendPathWith("companies");

        return this;
    }

    /**
     * Queries Estimize for the company information on the company specified with ticker.
     *
     * For example:
     *
     * <pre>
     *     Companies companies = queryBuilder.companies("MSFT").doGet(Companies.class);
     * </pre>
     *
     * @param ticker ie. "MSFT".
     */
    public QueryBuilder companies (String ticker) {

        extendPathWith(Companies.COMPANIES);
        extendPathWith(ticker);

        return this;
    }

    /**
     * Queries Estimize for the past financial releases for the specified company.
     *
     * <pre>
     *     
     * </pre>
     */
    public QueryBuilder releases () {

        extendPathWith(Releases.RELEASES);

        return this;
    }

    /**
     * 
     * @param year A four-digit year -- ie. 2012.
     *
     * @return
     */
    public QueryBuilder releases (String year) {

        releases ();
        extendPathWith(year);

        return this;
    }

    /**
     * 
     * @param year A four-digit year -- ie. 2012.
     *
     * @return
     */
    public QueryBuilder releases (String year, String quarter) {

        releases (year);
        extendPathWith(quarter);

        return this;
    }

    public QueryBuilder estimates () {

        extendPathWith(Estimates.ESTIMATES);

        return this;
    }

    /**
     * 
     * @param year A four-digit year -- ie. 2012.
     *
     * @return
     */
    public QueryBuilder estimates (String year) {

        estimates();
        extendPathWith(year);

        return this;
    }

    /**
     * 
     * @param year A four-digit year -- ie. 2012.
     *
     * @return
     */
    public QueryBuilder estimates (String year, String quarter) {

        estimates(year);
        extendPathWith(quarter);

        return this;
    }

    public QueryBuilder consensus () {

        extendPathWith(Consensuses.CONSENSUS);

        return this;
    }

    final RequestCallback requestCallback = new RequestCallback() {

        @Override
        public void doWithRequest(ClientHttpRequest request) throws IOException {
            request.getHeaders().add("Accept", "application/json");
        }
    };

    abstract class AbstractResponseExtractor<T> implements ResponseExtractor<T> {

        private final String topLevelNodeName, openNode, closeNode;

        public AbstractResponseExtractor (String topLevelNodeName) {
            this (
                topLevelNodeName,
                "{ \"" +  topLevelNodeName + "\" : { \"payload\" : ",
                "}}"
            );
        }

        public AbstractResponseExtractor (String topLevelNodeName, String openNode, String closeNode) {
            this.topLevelNodeName = topLevelNodeName;
            this.openNode = openNode;
            this.closeNode = closeNode;
        }

        @Override
        public T extractData(final ClientHttpResponse response) throws IOException {

            final InputStream inputStream = response.getBody();

            final SequenceInputStream sequenceInputStream =
                    new SequenceInputStream (
                        Collections.enumeration(
                            Arrays.asList(
                                new InputStream[] {
                                        new ByteArrayInputStream(openNode.getBytes()),
                                        inputStream,
                                        new ByteArrayInputStream(closeNode.getBytes())
                                    }
                                )
                            )
                    );

//            StringWriter writer = new StringWriter();
//            String encoding = "UTF-8";
//            IOUtils.copy(sequenceInputStream, writer, encoding);
//            System.out.println(writer.toString());

            HttpMessageConverter<?> messageConverter = getRestTemplate().getMessageConverters().get(0);

            MarshallingHttpMessageConverter marshallingHttpMessageConverter =
                (MarshallingHttpMessageConverter) messageConverter;

            HttpInputMessage inputMessage = new HttpInputMessage() {

                @Override
                public HttpHeaders getHeaders() {
                    return response.getHeaders();
                }

                @Override
                public InputStream getBody() throws IOException {
                    return sequenceInputStream;
                }
            };

            T result = null;

            if (canRead (marshallingHttpMessageConverter)) {
                result = thenRead (marshallingHttpMessageConverter, inputMessage);
            } else
                throw new ConversionException("Unable to convert the resultant JSON for the topLevelNodeName: "
                    + topLevelNodeName);

            return result;
        }

        protected abstract boolean canRead (MarshallingHttpMessageConverter marshallingHttpMessageConverter);

        protected abstract T thenRead (
            MarshallingHttpMessageConverter marshallingHttpMessageConverter,
            HttpInputMessage inputMessage
        ) throws IOException;
    };

    class CompaniesResponseExtractor extends AbstractResponseExtractor<Companies> {

        public CompaniesResponseExtractor() {
            super(Companies.COMPANIES);
        }

        @Override
        protected boolean canRead(MarshallingHttpMessageConverter marshallingHttpMessageConverter) {
            return marshallingHttpMessageConverter.canRead(Companies.class, MediaType.APPLICATION_JSON);
        }

        @Override
        protected Companies thenRead(
            MarshallingHttpMessageConverter marshallingHttpMessageConverter,
            HttpInputMessage inputMessage
        ) throws IOException {
            return (Companies) marshallingHttpMessageConverter.read(Companies.class, inputMessage);
        }
    };

    class ReleasesResponseExtractor extends AbstractResponseExtractor<Releases> {

        public ReleasesResponseExtractor() {
            super(Releases.RELEASES);
        }

        @Override
        protected boolean canRead(MarshallingHttpMessageConverter marshallingHttpMessageConverter) {
            return marshallingHttpMessageConverter.canRead(Releases.class, MediaType.APPLICATION_JSON);
        }

        @Override
        protected Releases thenRead(
            MarshallingHttpMessageConverter marshallingHttpMessageConverter,
            HttpInputMessage inputMessage
        ) throws IOException {
            return (Releases) marshallingHttpMessageConverter.read(Releases.class, inputMessage);
        }
    };

    class EstimatesResponseExtractor extends AbstractResponseExtractor<Estimates> {

        public EstimatesResponseExtractor() {
            super(Estimates.ESTIMATES);
        }

        @Override
        protected boolean canRead(MarshallingHttpMessageConverter marshallingHttpMessageConverter) {
            return marshallingHttpMessageConverter.canRead(Estimates.class, MediaType.APPLICATION_JSON);
        }

        @Override
        protected Estimates thenRead(
            MarshallingHttpMessageConverter marshallingHttpMessageConverter,
            HttpInputMessage inputMessage
        ) throws IOException {
            return (Estimates) marshallingHttpMessageConverter.read(Estimates.class, inputMessage);
        }
    };

    class ConsensusesResponseExtractor extends AbstractResponseExtractor<Consensuses> {

        public ConsensusesResponseExtractor() {
            super(
                Consensuses.CONSENSUSES,
                "{ \"" +  Consensuses.CONSENSUSES + "\" : ",
                "}"
            );
        }

        @Override
        protected boolean canRead(MarshallingHttpMessageConverter marshallingHttpMessageConverter) {
            return marshallingHttpMessageConverter.canRead(Consensuses.class, MediaType.APPLICATION_JSON);
        }

        @Override
        protected Consensuses thenRead(
            MarshallingHttpMessageConverter marshallingHttpMessageConverter,
            HttpInputMessage inputMessage
        ) throws IOException {
            return (Consensuses) marshallingHttpMessageConverter.read(Consensuses.class, inputMessage);
        }
    };

    @Override
    protected String getKey() {
        return getEscapedURI();
    }

    @Override
    protected <T> T doExecute(Class<T> type) {

        ResponseExtractor<T> responseExtractor = (ResponseExtractor<T>) responseExtractorMap.get(type);

        if (responseExtractor == null)
            throw new UnknownTypeException ("There is no response extractor associated with the type " + type + ".");

        String escapedURI = getEscapedURI();

        log.info ("escapedURI: " + escapedURI);

        return (T) getRestTemplate().execute(escapedURI, HttpMethod.GET, requestCallback, responseExtractor);
    }
}
