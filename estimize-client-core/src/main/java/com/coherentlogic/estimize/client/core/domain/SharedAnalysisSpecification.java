package com.coherentlogic.estimize.client.core.domain;

import java.math.BigDecimal;

/**
 *
 * @author <a href="support@coherentlogic.com">Support</a>
 */
public interface SharedAnalysisSpecification {

    public static final String FISCAL_YEAR = "fiscal_year", FISCAL_QUARTER = "fiscal_quarter",
        EARNINGS_PER_SHARE = "eps", REVENUE = "revenue";

    public String getFiscalYear();

    public void setFiscalYear(String fiscalYear);

    public int getFiscalQuarter();

    public void setFiscalQuarter(int fiscalQuarter);

    public BigDecimal getEarningsPerShare();

    public void setEarningsPerShare(BigDecimal earningsPerShare);

    public BigDecimal getRevenue();

    public void setRevenue(BigDecimal revenue);
}
