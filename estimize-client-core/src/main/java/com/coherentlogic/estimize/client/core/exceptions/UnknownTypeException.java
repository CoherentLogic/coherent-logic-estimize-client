package com.coherentlogic.estimize.client.core.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * An exception that is thrown when the type passed to the doGet method cannot be converted.
 *
 * @author <a href="support@coherentlogic.com">Support</a>
 */
public class UnknownTypeException extends NestedRuntimeException {

    public UnknownTypeException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public UnknownTypeException(String msg) {
        super(msg);
    }
}
