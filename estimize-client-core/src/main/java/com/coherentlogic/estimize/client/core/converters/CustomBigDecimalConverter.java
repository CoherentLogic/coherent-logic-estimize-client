package com.coherentlogic.estimize.client.core.converters;

import java.math.BigDecimal;

import com.coherentlogic.coherent.data.model.core.exceptions.MethodNotSupportedException;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * @author <a href="support@coherentlogic.com">Support</a>
 */
public class CustomBigDecimalConverter implements Converter {

    @Override
    public boolean canConvert(Class type) {
        return BigDecimal.class.equals(type);
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        throw new MethodNotSupportedException("The marshal method is not supported.");
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        String value = reader.getValue();

        return (value == null || "".equals(value)) ? null : new BigDecimal (value);
    }
}
