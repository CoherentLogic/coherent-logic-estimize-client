package com.coherentlogic.estimize.client.core.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * 
 * @author <a href="support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=Companies.COMPANIES)
@XStreamAlias(Companies.COMPANIES)
public class Companies extends IdentityBean {

    private static final long serialVersionUID = 1262300089528203268L;

    public static final String COMPANIES = "companies", PAYLOAD = "payload", COMPANY_LIST = "companyList";

    @XStreamImplicit
    @XStreamAlias(PAYLOAD)
    private List<Company> companyList;

    public Companies() {
        this (new ArrayList<Company> ());
    }

    public Companies(List<Company> companyList) {
        super();
        this.companyList = companyList;
    }

    @OneToMany(cascade=CascadeType.ALL)
    public List<Company> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(List<Company> companyList) {

        List<Company> oldValue = this.companyList;

        this.companyList = companyList;

        firePropertyChange(COMPANY_LIST, oldValue, companyList);
    }

    @Override
    public String toString() {
        return "Companies [companyList=" + companyList + "]";
    }
}
