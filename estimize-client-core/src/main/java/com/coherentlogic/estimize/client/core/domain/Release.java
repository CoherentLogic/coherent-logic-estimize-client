package com.coherentlogic.estimize.client.core.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;
import com.coherentlogic.estimize.client.core.converters.CustomBigDecimalConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 * 
 * @todo BigDecimal.ROUND_HALF_EVEN (probably)
 *
 * @author <a href="support@coherentlogic.com">Support</a>
 */
public class Release extends IdentityBean implements SharedAnalysisSpecification {

    private static final long serialVersionUID = -2631731727131267374L;

    static final String
        RELEASE_DATE = "release_date",
        WALL_STREET_EARNINGS_PER_SHARE_ESTIMATE = "wallstreet_eps_estimate",
        WALL_STREET_REVENUE_ESTIMATE= "wallstreet_revenue_estimate",
        CONSENSUS_EARNINGS_PER_SHARE_ESTIMATE = "consensus_eps_estimate",
        CONSENSUS_REVENUE_ESTIMATE= "consensus_revenue_estimate";

    @XStreamAlias(RELEASE_DATE)
    @XStreamAsAttribute
    private Date releaseDate;

    @XStreamAlias(WALL_STREET_EARNINGS_PER_SHARE_ESTIMATE)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal wallStreetEarningsPerShareEstimate;

    @XStreamAlias(WALL_STREET_REVENUE_ESTIMATE)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal wallStreetReveueEstimate;

    @XStreamAlias(CONSENSUS_EARNINGS_PER_SHARE_ESTIMATE)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal consensusEarningsPerShareEstimate;

    @XStreamAlias(CONSENSUS_REVENUE_ESTIMATE)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal consensusReveueEstimate;

    @XStreamAlias(FISCAL_YEAR)
    @XStreamAsAttribute
    private String fiscalYear;

    @XStreamAlias(FISCAL_QUARTER)
    @XStreamAsAttribute
    private int fiscalQuarter;

    @XStreamAlias(EARNINGS_PER_SHARE)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal earningsPerShare;

    @XStreamAlias(REVENUE)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal revenue;

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {

        Date oldValue = this.releaseDate;

        this.releaseDate = releaseDate;

        firePropertyChange("releaseDate", oldValue, releaseDate);
    }

    public BigDecimal getWallStreetEarningsPerShareEstimate() {
        return wallStreetEarningsPerShareEstimate;
    }

    public void setWallStreetEarningsPerShareEstimate(BigDecimal wallStreetEarningsPerShareEstimate) {

        BigDecimal oldValue = this.wallStreetEarningsPerShareEstimate;

        this.wallStreetEarningsPerShareEstimate = wallStreetEarningsPerShareEstimate;

        firePropertyChange("wallStreetEarningsPerShareEstimate", oldValue, wallStreetEarningsPerShareEstimate);
    }

    public BigDecimal getWallStreetReveueEstimate() {
        return wallStreetReveueEstimate;
    }

    public void setWallStreetReveueEstimate(BigDecimal wallStreetReveueEstimate) {

        BigDecimal oldValue = this.wallStreetReveueEstimate;

        this.wallStreetReveueEstimate = wallStreetReveueEstimate;

        firePropertyChange("wallStreetReveueEstimate", oldValue, wallStreetReveueEstimate);
    }

    public BigDecimal getConsensusEarningsPerShareEstimate() {
        return consensusEarningsPerShareEstimate;
    }

    public void setConsensusEarningsPerShareEstimate(BigDecimal consensusEarningsPerShareEstimate) {

        BigDecimal oldValue = this.consensusEarningsPerShareEstimate;

        this.consensusEarningsPerShareEstimate = consensusEarningsPerShareEstimate;

        firePropertyChange("consensusEarningsPerShareEstimate", oldValue, consensusEarningsPerShareEstimate);
    }

    public BigDecimal getConsensusReveueEstimate() {
        return consensusReveueEstimate;
    }

    public void setConsensusReveueEstimate(BigDecimal consensusReveueEstimate) {

        BigDecimal oldValue = this.consensusReveueEstimate;

        this.consensusReveueEstimate = consensusReveueEstimate;

        firePropertyChange("consensusReveueEstimate", oldValue, consensusReveueEstimate);
    }

    public String getFiscalYear() {
        return fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {

        String oldValue = this.fiscalYear;

        this.fiscalYear = fiscalYear;

        firePropertyChange("fiscalYear", oldValue, fiscalYear);
        
    }

    public int getFiscalQuarter() {
        return fiscalQuarter;
    }

    public void setFiscalQuarter(int fiscalQuarter) {

        int oldValue = this.fiscalQuarter;

        this.fiscalQuarter = fiscalQuarter;

        firePropertyChange("fiscalQuarter", oldValue, fiscalQuarter);
    }

    public BigDecimal getEarningsPerShare() {
        return earningsPerShare;
    }

    public void setEarningsPerShare(BigDecimal earningsPerShare) {

        BigDecimal oldValue = this.earningsPerShare;

        this.earningsPerShare = earningsPerShare;

        firePropertyChange("earningsPerShare", oldValue, earningsPerShare);
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {

        BigDecimal oldValue = this.revenue;

        this.revenue = revenue;

        firePropertyChange("revenue", oldValue, revenue);
    }

    @Override
    public String toString() {
        return "Release [releaseDate=" + releaseDate + ", wallStreetEarningsPerShareEstimate="
            + wallStreetEarningsPerShareEstimate + ", wallStreetReveueEstimate=" + wallStreetReveueEstimate
            + ", consensusEarningsPerShareEstimate=" + consensusEarningsPerShareEstimate
            + ", consensusReveueEstimate=" + consensusReveueEstimate + ", fiscalYear=" + fiscalYear
            + ", fiscalQuarter=" + fiscalQuarter + ", earningsPerShare=" + earningsPerShare + ", revenue=" + revenue
            + "]";
    }
}
