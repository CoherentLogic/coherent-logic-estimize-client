package com.coherentlogic.estimize.client.core.converters;

import com.coherentlogic.estimize.client.core.domain.Estimate;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * @deprecated
 */
public class EstimateConverter implements Converter {

    @Override
    public boolean canConvert(Class type) {
        return Estimate.class.equals(type);
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        throw new UnsupportedOperationException("The marshal method is not supported.");
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        throw new UnsupportedOperationException("The unmarshal method is not supported.");

//        Estimate result = new Estimate ();
//
//        context.convertAnother(result, SharedAnalysisBean.class);
//
//        return result;
    }
}
