package com.coherentlogic.estimize.client.core.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;
import com.coherentlogic.estimize.client.core.converters.CustomBigDecimalConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;

@Entity
@Table(name=Revision.REVISIONS)
public class Revision extends IdentityBean {

    private static final long serialVersionUID = 2877974475661801354L;

    static final String REVISIONS = "revisions";

    @XStreamAlias(EarningsPerShare.MEAN)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal mean;

    @XStreamAlias(EarningsPerShare.HIGH)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal high;

    @XStreamAlias(EarningsPerShare.LOW)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal low;

    @XStreamAlias(EarningsPerShare.STANDARD_DEVIATION)
    @XStreamAsAttribute
    @XStreamConverter(CustomBigDecimalConverter.class)
    private BigDecimal standardDeviation;

    @XStreamAlias(EarningsPerShare.COUNT)
    @XStreamAsAttribute
    private Integer count;

    /**
     * @deprecated Need to change this to date.
     */
    @XStreamAlias(EarningsPerShare.UPDATED_AT)
    @XStreamAsAttribute
    private String updatedAt;

    public BigDecimal getMean() {
        return mean;
    }

    public void setMean(BigDecimal mean) {

        BigDecimal oldValue = this.mean;

        this.mean = mean;

        firePropertyChange(EarningsPerShare.MEAN, oldValue, mean);
    }

    public BigDecimal getHigh() {
        return high;
    }

    public void setHigh(BigDecimal high) {

        BigDecimal oldValue = this.high;

        this.high = high;

        firePropertyChange(EarningsPerShare.HIGH, oldValue, high);
    }

    public BigDecimal getLow() {
        return low;
    }

    public void setLow(BigDecimal low) {

        BigDecimal oldValue = this.low;

        this.low = low;

        firePropertyChange(EarningsPerShare.LOW, oldValue, low);
    }

    public BigDecimal getStandardDeviation() {
        return standardDeviation;
    }

    public void setStandardDeviation(BigDecimal standardDeviation) {

        BigDecimal oldValue = this.standardDeviation;

        this.standardDeviation = standardDeviation;

        firePropertyChange(EarningsPerShare.STANDARD_DEVIATION, oldValue, standardDeviation);
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {

        Integer oldValue = this.count;

        this.count = count;

        firePropertyChange(EarningsPerShare.COUNT, oldValue, count);
    }

    /**
     * @deprecated Need to change this to date.
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * @deprecated Need to change this to date.
     */
    public void setUpdatedAt(String updatedAt) {

        String oldValue = this.updatedAt;

        this.updatedAt = updatedAt;

        firePropertyChange(EarningsPerShare.UPDATED_AT, oldValue, updatedAt);
    }

    @Override
    public String toString() {
        return "Revision [mean=" + mean + ", high=" + high + ", low=" + low + ", standardDeviation=" + standardDeviation
            + ", count=" + count + ", updatedAt=" + updatedAt + "]";
    }
}
