package com.coherentlogic.estimize.client.core.domain;

import java.util.List;

public interface RevisionSpecification {

    static final String REVISION_LIST = "revisionList";

    List<Revision> getRevisionList();

    void setRevisionList(List<Revision> revisionList);
}
