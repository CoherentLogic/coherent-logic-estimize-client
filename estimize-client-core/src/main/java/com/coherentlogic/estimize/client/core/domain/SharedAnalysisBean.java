package com.coherentlogic.estimize.client.core.domain;

import java.math.BigDecimal;

import com.coherentlogic.coherent.data.model.core.domain.IdentityBean;
import com.coherentlogic.estimize.client.core.converters.SharedAnalysisBeanConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 * @deprecated XStream doesn't seem to handle inheritace well so this is being removed.
 *
 * @author <a href="support@coherentlogic.com">Support</a>
 */
//@XStreamConverter(SharedAnalysisBeanConverter.class)
public class SharedAnalysisBean extends IdentityBean {

    static final String FISCAL_YEAR = "fiscal_year", FISCAL_QUARTER = "fiscal_quarter", EARNINGS_PER_SHARE = "eps",
        REVENUE = "revenue";

    @XStreamAlias(FISCAL_YEAR)
    @XStreamAsAttribute
    private String fiscalYear;

    @XStreamAlias(FISCAL_QUARTER)
    @XStreamAsAttribute
    private int fiscalQuarter;

    @XStreamAlias(EARNINGS_PER_SHARE)
    @XStreamAsAttribute
    private BigDecimal earningsPerShare;

    @XStreamAlias(REVENUE)
    @XStreamAsAttribute
    private BigDecimal revenue;

    public String getFiscalYear() {
        return fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {

        String oldValue = this.fiscalYear;

        this.fiscalYear = fiscalYear;

        firePropertyChange("fiscalYear", oldValue, fiscalYear);
        
    }

    public int getFiscalQuarter() {
        return fiscalQuarter;
    }

    public void setFiscalQuarter(int fiscalQuarter) {

        int oldValue = this.fiscalQuarter;

        this.fiscalQuarter = fiscalQuarter;

        firePropertyChange("fiscalQuarter", oldValue, fiscalQuarter);
    }

    public BigDecimal getEarningsPerShare() {
        return earningsPerShare;
    }

    public void setEarningsPerShare(BigDecimal earningsPerShare) {

        BigDecimal oldValue = this.earningsPerShare;

        this.earningsPerShare = earningsPerShare;

        firePropertyChange("earningsPerShare", oldValue, earningsPerShare);
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void setRevenue(BigDecimal revenue) {

        BigDecimal oldValue = this.revenue;

        this.revenue = revenue;

        firePropertyChange("revenue", oldValue, revenue);
    }

    @Override
    public String toString() {
        return "SharedAnalysisBean [fiscalYear=" + fiscalYear + ", fiscalQuarter=" + fiscalQuarter
                + ", earningsPerShare=" + earningsPerShare + ", revenue=" + revenue + "]";
    }
}
