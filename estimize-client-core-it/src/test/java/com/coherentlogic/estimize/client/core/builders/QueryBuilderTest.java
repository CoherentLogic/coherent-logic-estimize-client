package com.coherentlogic.estimize.client.core.builders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;

import com.coherentlogic.estimize.client.core.configuration.GlobalConfiguration;
import com.coherentlogic.estimize.client.core.configuration.XMLConfiguration;
import com.coherentlogic.estimize.client.core.domain.Companies;
import com.coherentlogic.estimize.client.core.domain.Consensuses;
import com.coherentlogic.estimize.client.core.domain.Estimates;
import com.coherentlogic.estimize.client.core.domain.Releases;

/**
 * @author <a href="support@coherentlogic.com">Support</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={GlobalConfiguration.class, XMLConfiguration.class})
@SpringBootApplication(scanBasePackages = {"com.coherentlogic.estimize.client"})
public class QueryBuilderTest {

    @Autowired
    private QueryBuilder queryBuilder;

    @After
    public void tearDown () {
        this.queryBuilder = null;
    }

    /**
     * Running these tests back-to-back can result in a 404.
     */
    @Test
    public void testGetCompanies () {

        Companies companies = queryBuilder.companies().doGet(Companies.class);

        assertTrue (2000 < companies.getCompanyList().size());
    }

    @Test
    public void testGetCompaniesWithTickerMSFT () {

        Companies companies = queryBuilder.companies("MSFT").doGet(Companies.class);

        assertEquals (1, companies.getCompanyList().size());
    }

    /**
     * Expect a 422 but received a 404.
     */
    @Test
    public void testGetCompaniesWithBadTicker () {

        Companies companies;

        try {
            companies = queryBuilder.companies("M--Z").doGet(Companies.class);
        } catch (HttpClientErrorException hcee) {
            if (hcee.getStatusCode().equals(HttpStatus.NOT_FOUND))
                return;
            else throw hcee;
        }
    }

    @Test
    public void testGetReleases () {

        Releases releases = queryBuilder.companies("MSFT").releases().doGet(Releases.class);

        assertTrue (1 <= releases.getReleaseList().size());
    }

    @Test
    public void testGetReleasesFor2015 () {

        Releases releases = queryBuilder.companies("MSFT").releases("2015").doGet(Releases.class);

        assertTrue (1 <= releases.getReleaseList().size());
    }

    @Test
    public void testGetReleasesFor2015Q4 () {

        Releases releases = queryBuilder.companies("MSFT").releases("2015", "4").doGet(Releases.class);

        assertTrue (1 <= releases.getReleaseList().size());
    }

    @Test
    public void testGetEstimates () {

        Estimates estimates = queryBuilder.companies("MSFT").estimates().doGet(Estimates.class);

        assertTrue (1 <= estimates.getEstimateList().size());
    }

    @Test
    public void testGetEstimatesWithBadTicker () {
        try {
            queryBuilder.companies("M#FT").estimates().doGet(Estimates.class);
        } catch (HttpClientErrorException hcee) {
            if (hcee.getStatusCode().equals(HttpStatus.NOT_FOUND))
                return;
            else throw hcee;
        }
    }

    @Test
    public void testGetEstimatesFor2015 () {

        Estimates estimates = queryBuilder.companies("MSFT").estimates("2015").doGet(Estimates.class);

        assertTrue (1 <= estimates.getEstimateList().size());
    }

    @Test
    public void testGetEstimatesFor2015Q3 () {

        Estimates estimates = queryBuilder.companies("MSFT").estimates("2015", "3").doGet(Estimates.class);

        assertTrue (1 <= estimates.getEstimateList().size());
    }

    @Test
    public void testGetConsensuses () {

        Consensuses consensuses =
            queryBuilder.releases("535c963053c804e0d50002a1").consensus().doGet(Consensuses.class);

        assertNotNull (consensuses.getEstimizeConsensus().getEarningsPerShare().getHigh());
        assertNotNull (consensuses.getEstimizeConsensus().getRevenue().getHigh());
        assertNotNull (consensuses.getWallStreetConsensus().getEarningsPerShare().getHigh());
        assertNotNull (consensuses.getWallStreetConsensus().getRevenue().getHigh());
    }
}
