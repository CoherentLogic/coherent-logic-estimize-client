package com.coherentlogic.estimize.client.core.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.coherentlogic.estimize.client.core.configuration.GlobalConfiguration.X_ESTIMIZE_KEY;

import com.coherentlogic.coherent.data.model.core.exceptions.FrameworkMisconfiguredException;

/**
 * 
 * @author <a href="support@coherentlogic.com">Support</a>
 */
@Configuration
public class IntegrationTestConfiguration {

    @Bean
    @Qualifier(X_ESTIMIZE_KEY)
    public String getXEstimizeKey () {

        String xEstimizeKey = System.getenv(X_ESTIMIZE_KEY);

        if (xEstimizeKey == null)
            throw new FrameworkMisconfiguredException("In order to run the integration tests the " + X_ESTIMIZE_KEY
                + " must be set to a non-null value.");

        return xEstimizeKey;

    }
}
